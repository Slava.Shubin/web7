function updatePrice() {
	let price = 0;
	let prices = getPrices();
	let count = parseInt(document.getElementById("number").value);
	//document.getElementById("number").value;

	let s = document.getElementsByName("productType");
	let select = s[0];
	let priceIndex = parseInt(select.value) - 1;
    if (priceIndex >= 0) {
        price = prices.productTypes[priceIndex]*count;
    }
	
	// Ð¡Ð¼Ð¾ÑÑÐ¸Ð¼ ÐºÐ°ÐºÐ°Ñ ÑÐ¾Ð²Ð°ÑÐ½Ð°Ñ Ð¾Ð¿ÑÐ¸Ñ Ð²ÑÐ±ÑÐ°Ð½Ð°.
	let radios = document.getElementsByName("productOptions");
	radios.forEach(function(radio) {
  		if (radio.checked) {
    		let optionPrice = prices.productOptions[radio.value];
    		if (optionPrice !== undefined) {
      			price += optionPrice;
    		}
  		}
	});

	// Ð¡Ð¼Ð¾ÑÑÐ¸Ð¼ ÐºÐ°ÐºÐ¸Ðµ ÑÐ¾Ð²Ð°ÑÐ½ÑÐµ ÑÐ²Ð¾Ð¹ÑÑÐ²Ð° Ð²ÑÐ±ÑÐ°Ð½Ñ.
	  let checkboxes = document.querySelectorAll("#checkboxes input");
	  checkboxes.forEach(function(checkbox) {
	    if (checkbox.checked) {
	      let propertyPrice = prices.productProperties[checkbox.name];
	      if (propertyPrice !== undefined) {
	        price += propertyPrice;
	      }
	    }
	  });

	let productPrice = document.getElementById("productPrice");
 	productPrice.innerHTML = price + " ÑÑÐ±Ð»ÐµÐ¹";
 	productPrice.style.fontWeight = "bold";

}

function getPrices() {
  return {
    productTypes: [2190, 2890, 4790, 1490, 2390, 1790, 1990, 3390, 3590],
    productOptions: {
      option1: 890,
      option2: 690,
      option3: 490,
    },
    productProperties: {
    	properties: 390,
    }
  };
}



	//Ð¡ÐºÑÑÐ²Ð°ÐµÐ¼ Ð¸Ð»Ð¸ Ð¿Ð¾ÐºÐ°Ð·ÑÐ²Ð°ÐµÐ¼ ÑÐ°Ð´Ð¸Ð¾ÐºÐ½Ð¾Ð¿ÐºÐ¸
    window.addEventListener('DOMContentLoaded', function (event) {
    	let radioDiv = document.getElementById("radios");
  		radioDiv.style.display = "none";

  		let checkboxesDiv = document.getElementById("checkboxes");
  		checkboxesDiv.style.display = "none";

  		//ÐÐ°ÑÐ¾Ð´Ð¸Ð¼ select
	  let product = document.getElementsByName("productType");
	  

	  //
	  product[0].addEventListener("change", function(event) {
	    let select = event.target;
	    console.log(select.value);
	    updatePrice();
	    let radios = document.getElementById("radios");
	    //console.log(select.value);
	    // ÐÐ¾Ð¶Ð½Ð¾ Ð¸ÑÐ¿Ð¾Ð»ÑÐ·Ð¾Ð²Ð°ÑÑ getElementsByClassName()
	    if (select.value == "1") {
	      radios.style.display = "none";
	      checkboxes.style.display = "none";
	    }
	    if (select.value == "2") {
	      radios.style.display = "flex";
	      checkboxes.style.display = "none";
	    }
	    if (select.value == "3") {
	      radios.style.display = "none";
	      checkboxes.style.display = "flex";
	    }
	    if (select.value >= "4") {
	      radios.style.display = "flex";
	      checkboxes.style.display = "flex";
	    }
	  });

	  //ÐÐ°Ð·Ð½Ð°ÑÐ°ÐµÐ¼ Ð¾Ð±ÑÐ°Ð±Ð¾ÑÑÐ¸Ðº ÑÐ°Ð´Ð¸Ð¾ÐºÐ½Ð¾Ð¿Ð¾Ðº
		let r = document.querySelectorAll("#radios input[type=radio]");
	  	r.forEach(function(radio) {
	    radio.addEventListener("change", function(event) {
	      let r = event.target;
	      console.log(r.value);
	      updatePrice();
	    });
	  //ÐÐ°Ð·Ð½Ð°ÑÐ°ÐµÐ¼ Ð¾Ð±ÑÐ°Ð±Ð¾ÑÑÐ¸Ðº ÑÐµÐºÐ±Ð¾ÐºÑÐ°
	  let checkboxes = document.querySelectorAll("#checkboxes input");
	  	checkboxes.forEach(function(checkbox) {
	    checkbox.addEventListener("change", function(event) {
	      let c = event.target;
	      console.log(c.name);
	      console.log(c.value);
	      updatePrice();
	      });
	  	});
	  document.getElementById("number").addEventListener("input", updatePrice);

  });
	updatePrice();
  });
